//
// Created by crypt on 26/06/16.
//

#ifndef PREPARATION_TREENODE_H
#define PREPARATION_TREENODE_H

#include <memory>
#include <iostream>
#include <vector>
#include <sstream>

using namespace std;
namespace interview {
    template<typename Payload>
    class BinaryTreeNode {
    public:
        BinaryTreeNode<Payload>(Payload payload) : data(payload), height(0) { };


        void add(Payload payload) {
            if (payload < data) {

                if (left) {
                    left->add(payload);
                    if (left->height == height) {
                        height++;
                    }
                } else {
                    left = addLeaf(payload);
                }
            } else if (payload > data) {

                if (right) {
                    right->add(payload);
                    if (right->height == height) {
                        height++;
                    }
                } else {
                    right = addLeaf(payload);
                }
            }
        }

        inline unsigned getHeight() { return this->height; }

    private:

        shared_ptr<BinaryTreeNode<Payload>> addLeaf(Payload payload) {
            shared_ptr<BinaryTreeNode<Payload>> node;
            node = make_shared<BinaryTreeNode<Payload>>(payload);

            if (left == nullptr && right == nullptr) {
                this->height = 1;
            }
            return node;
        }

        unsigned height;
        Payload data;
        /*
        shared_ptr<BinaryTreeNode<Payload>> parent;
                */
        shared_ptr<BinaryTreeNode<Payload>>
                left;
        shared_ptr<BinaryTreeNode<Payload>>
                right;

        void stream(vector<ostringstream> &lines) {
            //const ostringstream &elm = lines[height];
            ostream &elm = lines[height];
            for(int i=0; i < height; i++) {
                elm << "       ";
            }
            elm << " N[" << data << "]" << "@" << height << " ";
            if (left) {
                left->stream(lines);
            } else if (height > 0){
                ostream &elm2 = lines[height - 1];
                elm2 << " _____ ";
            }
            if (right) {
                right->stream(lines);
            } else if (height > 0){
                ostream &elm2  = lines[height -1];
                elm2 << " _____ ";
            }
        }

        friend ostream &operator<<(ostream &ost, BinaryTreeNode<Payload> &node) {

            vector<ostringstream> lines;
            for (int i = 0; i <= node.height; i++) {
                lines.emplace_back(ostringstream());
            }
            node.stream(lines);

            for (int i = lines.size(); i >= 0; i--) {
                auto &line = lines[i];
                ost << line.str() << endl;
            }
            /*
            ost << "N[" << node.data << "]" << "@" << node.height;

            if (node.left) {
                node.left->stream(ost)
                //ost << endl << "L" << *(node.left);
            }
            if (node.right) {
                ost << endl << "R" << *(node.right);
            }*/
            return ost;
        }
    };

}

#endif //PREPARATION_TREENODE_H
