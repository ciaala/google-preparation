#include "stdio.h"

int hexdigit(const char hex ) {
	if ( hex >= 0x30 && hex <= 0x39) 
		return hex - 0x30;
	if ( hex > 0x40 && hex <= 0x46 )
		return 10 + hex-0x41;
	if ( hex > 0x60 && hex <= 0x66 )
		return 10 + hex-0x61;
}

int cmpstrtohex(const char* str, const char* hex ) { 
	char value = 16* hexdigit(*hex) + hexdigit(*++hex);
	printf( "%c == %c\n",*str ,value);

	while ( value == *str && *str  && *hex) {
		++str;
		++hex;
		value = 16* hexdigit(*hex) + hexdigit(*++hex);
		printf( "%c == %c\n", *str ,value);

	}
	if ( *str || !*hex)
		return *str;
	if ( !*str || *hex ) 
		return 16* *hex + *(++hex);
	if ( *str != value )
		return *str-value;
	return 0;
}

unsigned int strtohex( char* out, const char* in ) {
	int i =0 ;
	while ( in[i] ){
		sprintf(out,"%02x",in[i]);	
		i++;
		out+=2;
	}

	out = 0;
	return i;
}


int main(int argc, char**argv) {
	char out[1024];
	char in[1024] = "Ciccio1234%^#@&"; 
	unsigned int len = strtohex(out,in);
	unsigned int diff = cmpstrtohex( in, out);
	printf("in : %s\nout: %s\nlen: %u\ndif: %u\n", in, out, len,diff );
	
	return 0;
}	
