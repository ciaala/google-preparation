#include "BasicDataClass/TreeNode.h"

#include <iostream>
#include <sstream>
#include <unordered_map>
#include <vector>
#include <algorithm>
#include <memory>

using namespace std;
using namespace interview;

int main() {
    cout << "Starting" << endl;
    shared_ptr<BinaryTreeNode<int>> root = make_shared<BinaryTreeNode<int>>(0);
    root->add(-1);
    root->add(1);
    root->add(2);
    root->add(-2);
    root->add(-3);
    root->add(3);
    root->add(-4);
    root->add(4);
    root->add(-5);
    root->add(5);
    cout << "Tree Height: " << root->getHeight() << endl;
    cout << *root;
}