#include <iostream>
#include <unordered_map>
#include <fstream>
#include <vector>

#define READ_BLOCK 1024

using namespace std;

namespace ffi {
    class AnalyseFile {
    private:
        string filename;
        unsigned long fileSize;

    private:
        unsigned long minimalBlockAppearance = 2;

        unordered_map<unsigned long, unsigned long> sequence;

        unordered_map<unsigned long, unsigned long> matched_tokens;


        unsigned long filesize(const string &filename) {
            std::ifstream in(filename, std::ifstream::ate | std::ifstream::binary);
            return (unsigned long) in.tellg();
        }

    public:

        void sequenceFile() {
            ifstream file(filename, ios::in | ios::binary);

            if (!file.is_open()) {
                throw runtime_error("Couldn't open file: " + filename);
            } else {
                char data[READ_BLOCK] = {0};
                unsigned long block;
                file.read((char *) &block, 8);

                sequence[block] = 1;
                unsigned long already_read = 8;
                unsigned long percentage = 0;
                while (!file.eof()) {


                    file.read(data, READ_BLOCK);
                    unsigned int read = ((unsigned int) file.gcount());
                    already_read += read;
                    for (int i = 0; i < read; i++) {
                        for (int j = 0; j < 8; j++) {
                            block = block << 1;
                            block = block | (data[i] & (1 << j));
                            unsigned long count = sequence.count(block) ? sequence[block] + 1 : 1;
                            sequence[block] = count;
                        }
                    }
                    unsigned long newPercentage = (100 * already_read) / fileSize;
                    if (percentage < newPercentage) {
                        percentage = newPercentage;
                        cout << "\rread " << percentage << "%" << flush;
                    }
                }
                file.close();
            }
            cout << "\rAnalyse completed" << endl;
        }


    public:
        AnalyseFile(const string &filename) : filename(filename) {
            this->fileSize = filesize(filename);
            cout << "File size: " << fileSize << endl;
        }

        double estimate() {
            long residual_bits = 0;
            unsigned long tokens = 0;
            if (sequence.size() == 0) {
                sequenceFile();
            }
            matched_tokens.clear();
            ifstream file(filename, ios::in | ios::binary);
            if (!file.is_open()) {
                throw runtime_error("Couldn't open file: " + filename);
            } else {
                char data[READ_BLOCK];
                unsigned long block;
                file.read((char *) &block, 8);
                //sequence[block] = 1;
                unsigned long already_read = 8;


                unsigned long percentage = 0;
                while (!file.eof()) {
                    file.read(data, READ_BLOCK);
                    unsigned int read = ((unsigned int) file.gcount());
                    already_read += read;
                    for (int k = 0; k < (read * 8);) {
                        int i = k / 8;
                        int j = k % 8;
                        block = block << 1;
                        block = block | (data[i] & (1 << j));
                        if (sequence.count(block) && sequence.at(block) >= minimalBlockAppearance) {
                            k += 8;
                            tokens += 1;
                            matched_tokens[block] = matched_tokens.count(block) ? matched_tokens[block] : 1;
                        } else {
                            k++;
                            residual_bits++;
                        }
                    }

                    unsigned long newPercentage = (100 * already_read) / fileSize;
                    if (percentage < newPercentage) {
                        percentage = newPercentage;
                        cout << "\restimated " << percentage << "%" << flush;
                    }
                }
                cout << "\rMatched unique tokens: " << matched_tokens.size() << endl;
                cout << "Found tokens: " << tokens << endl;
                double result = residual_bits;
                result /= (fileSize * 8);
                file.close();
                return result;
            }
            return 100;
        }

        unsigned long getMinimalBlockAppearance() const {
            return minimalBlockAppearance;
        }

        void setMinimalBlockAppearance(unsigned long minimalBlockAppearance) {
            AnalyseFile::minimalBlockAppearance = minimalBlockAppearance;
        }

    };

}

int main() {
    vector<string> filenames = {
            //"/home/crypt/Downloads/20141226_110811_Pano.jpg",
            //"/home/crypt/Downloads/2012-05-05 19.04.03.jpg",
            //"/home/crypt/Downloads/this-1200-machine-lets-you-make-untraceable-semi-automatic-rifles-at.htm",
            "/home/crypt/Dropbox/Camera Uploads/2012-01-13 12.38.56.jpg"
    };
    //;
    //const string filename = "/home/crypt/test.txt";
    //const string filename = "/home/crypt/Downloads/emmc_installer.zip";
    cout << "long size: " << sizeof(unsigned long) << endl;
    for (int i = 0; i < filenames.size(); i++) {
        const string &filename = filenames[i];
        ffi::AnalyseFile af = ffi::AnalyseFile(filename);
        for (unsigned long k = 2; k < 10003; k += 200) {
            af.setMinimalBlockAppearance(k);
            cout << filename << " > filter: " << k << ", compression ratio: " << af.estimate() << endl;
        }
        cout << endl;
    }

    return 0;
}